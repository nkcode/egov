package {
  public class test_xml_kaz {	
	[Embed(source="embed/test_kaz.xml", mimeType="application/octet-stream")]
	public const testXML:Class;
	public var test;

	function test_xml_kaz() {
		XML.ignoreWhitespace = true;
		test = XML(new testXML());		
	}
	
  }
}