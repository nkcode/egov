﻿package
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	public class typewriter
    {
		public var txtTimer:Timer = null;
		private var textArray:Array = new Array();
		private var t1:String;
		private var counter:Number = 0;
		private var textPath:Object
		private var stopped:Boolean = false;
		private var timerCreated:Boolean = false;
		public function typewriter() {
			
		}
		public function writeIt(_inputTxt:String, _textPath:Object, _appendInt:Number) {
		        trace("typewriter writeIt")
				textPath = _textPath;
				var inputTxt:String = _inputTxt;
				var appendInt:Number = _appendInt;
				textArray = [];
				textArray.length = 0;
				t1 = inputTxt;
				textArray = t1.split("");
				counter = 0;
				textPath.text = "";
				if(txtTimer!=null) {
				  trace("timer stop")
				  txtTimer.stop()
				}
 			    txtTimer = null;
				trace("creating timer");
				txtTimer = new Timer(appendInt, textArray.length);
				txtTimer.addEventListener(TimerEvent.TIMER, appendText);
				txtTimer.addEventListener(TimerEvent.TIMER_COMPLETE, txtTimerStop);
				txtTimer.start();
		}

		private function appendText(event:TimerEvent):void {
			textPath.appendText(textArray[counter]);
			counter++;
		}
		public function txtTimerStop(e:TimerEvent) {
			trace("stop")
			stopped = true;
		}
    }


}