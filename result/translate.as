﻿package {
  public class translate {	
	[Embed(source="embed/translate.xml", mimeType="application/octet-stream")]
	public const translateXML:Class;
	public var translate_xml;
	var dict:Object;
	public var cur_locale = "rus";

	function translate() {
		XML.ignoreWhitespace = true;
		translate_xml = XML(new translateXML());		
		dict = new Object();
		for each(var line in translate_xml.line) {
		   var l = new Object()
           l['rus'] = line.rus
		   l['kaz'] = line.kaz
		   dict[line.@id] = l;
		}
	}

	public function get_translation(id) {
	  var line = dict[id]
	  return line[cur_locale]              
	}
	
  }
}