﻿package
{
    import flash.display.MovieClip;
	import flash.display.FrameLabel;

    public class lesson
    {
		public function lesson(mc:MovieClip) {
			
			    mc.lesson_progress = function() {
					trace("LESSON PROGRESS")
					var chapter_index = mc.cur_chapter_index();
					var chapter_start_frame = mc.chapter_frames[chapter_index];
					trace("chapter start frame: "+chapter_start_frame)
					
					var start_frame = -1
					trace("key frames: "+mc.key_frames)					
					for each(var frame in mc.key_frames) {
						trace("FRAME: "+frame)
						if(frame==chapter_start_frame) {
							trace("FOUND")
							start_frame = 0;
						}
						if(frame>=(mc.my_cur_frame || mc.currentFrame)) {
							break;
						}
						if(start_frame>-1) {
							start_frame++
						}
					}
					var chapter_frames_num = mc.chapter_key_frames_num[chapter_index];
					trace("chapter frame: "+start_frame+"/"+chapter_frames_num)
					return chapter_index/(mc.chapter_frames.length)+(start_frame/chapter_frames_num)*1/(mc.chapter_frames.length);
				}
				mc.doFrameFlash = function() {
					var i = 0;
					for each(var num in mc.key_frames) {
						if(num==mc.currentFrame) {
							if(mc.frame_labels[i].toString().indexOf("f")!=-1) {
								return true;
							} else {
								return false;
							}
						}
						i++;
					}
					return false;
				}
			    mc.cur_chapter_index = function() {
					trace("LESSON.CUR_CHAPTER_INDEX")
					var cur_frame = mc.my_cur_frame || mc.currentFrame;
					trace("current frame:"+cur_frame)
					var cur_chapter = -1;
					for each(var chapter_start_frame in mc.chapter_frames) {
						trace("chapter start frame: "+chapter_start_frame)
						if(cur_frame<chapter_start_frame) {
							trace("current chapter index:"+cur_chapter)
							return cur_chapter;
						}
						cur_chapter++;
					}	
					return cur_chapter;
				}
                   
			    mc.key_frames= []
				mc.chapter_frames= []
				mc.chapter_key_frames_num=[]
				mc.frame_labels = []
			
				var labels:Array = mc.currentLabels;
				var per_chapter = 0;
				var chapter = 0;
				for (var i:uint = 0; i < labels.length; i++) {
				    
					var label:FrameLabel = labels[i];
					trace("frame " + label.frame + ": " + label.name);
					mc.key_frames.push(label.frame)
					mc.frame_labels.push(label.name)
					if(label.name.indexOf("ch")!=-1) {
						if(chapter>0) {
							trace("keyframes in prev chapter : "+per_chapter)
							mc.chapter_key_frames_num.push(per_chapter);
							per_chapter=0
						}
						chapter++
						trace("chapter frame "+label.frame)
						mc.chapter_frames.push(label.frame)						
					}
					per_chapter++;
					
				}
 			    trace("keyframes in prev chapter: "+per_chapter)
				mc.chapter_key_frames_num.push(per_chapter);
				trace("CHAPTERS FOUND IN LESSON:"+mc.chapter_frames.length);
				trace(mc.chapter_frames)

		}
    }

}

