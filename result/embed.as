package {
  public class embed {	
	[Embed(source="embed/menu.xml", mimeType="application/octet-stream")]
	public const menuXML:Class;
	public var menu;

	[Embed(source="embed/menu_kaz.xml", mimeType="application/octet-stream")]
	public const menu_kazXML:Class;
	public var menu_kaz;


	function embed() {
		XML.ignoreWhitespace = true;
		menu = XML(new menuXML());
		menu_kaz = XML(new menu_kazXML());
	}
	
  }
}