﻿package
{
    import type_effect;
    import flash.display.MovieClip;

    public class keyboard_input
    {
		public function keyboard_input(mc,opts:Object=null) {
		  (new type_effect(mc,{interval:60,per_tick:1,cursor:true,onStop:opts.onStop,keep_cursor:opts.keep_cursor})).restart();
		}
    }

}

