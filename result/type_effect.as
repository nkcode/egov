﻿package
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;


    public class type_effect
    {
		public var txtTimer:Timer = null;
		private var textArray:Array = new Array();
		private var loop_mode:Boolean = false;
		private var pos:Number = 0;
		private var text_element:Object
		private var stopped:Boolean = false;
		private var timerCreated:Boolean = false;
		private var chars_per_tick = 10;
		private var opts:Object;
		
		public function type_effect(text_element,opts:Object=null) {
		   trace("type_effect create")
           textArray = text_element.text.split("");
		   if(!opts) opts = new Object();
		   if(!opts.per_tick)
			   opts.per_tick = 10;			   
           
		   if(opts.total) {
			   opts.interval = opts.total/(textArray.length/opts.per_tick);
			   trace("interval "+opts.interval+" calculated from total: "+opts.total)
		   } else {
			   if(!opts.interval) 
				   opts.interval = 30;
		   }
		   this.opts = opts;
		   trace("per tick: "+opts.per_tick + " interval: "+opts.interval + "symbols: "+textArray.length)
		   
		   txtTimer = new Timer(opts.interval);
		   this.text_element = text_element;
		   this.chars_per_tick = opts.per_tick;
		   pos = 0;
		   txtTimer.addEventListener(TimerEvent.TIMER, appendText);
		   txtTimer.stop()
		}
		public function restart() {
		  txtTimer.stop();
		  trace("restart")
		  clear_text();
		  pos = 0;
		  loop_mode = false;
		  txtTimer.start();
		}
		
		public function loop() {
		  restart();	
	      loop_mode = true;
		}
		public function clear_text() {
			text_element.text = ""
		}

        private function appendText(event:TimerEvent):void {
try {
		  for(var count=0;count<this.chars_per_tick;count++) {
			  if(this.opts.cursor) {
				this.text_element.text = this.text_element.text.substr(0,-1)+textArray[pos]+"|";
			  } else {				
			    this.text_element.appendText(textArray[pos]);
			  }
			  pos++;
			  if(pos>=textArray.length) {
				if(loop_mode) {
					clear_text();
					trace("type_effect: loop restart");
					loop();
				} else {
					trace("type_effect: stop")
					//remove cursor
					if(this.opts.cursor) {
					  if(!this.opts.keep_cursor) {
					    this.text_element.text = this.text_element.text.substr(0,-1)
					  }
					}
					txtTimer.stop();
					if(this.opts.onStop) {
						this.opts.onStop()
					}
					break;
				}
			  }
		  }
} catch (e:Error) {
	txtTimer.stop();
}			
		}
		
    }

}