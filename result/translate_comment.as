﻿package {
  import flash.display.MovieClip;
  public class translate_comment {	

	function translate_comment(mc,id) {
	  mc = MovieClip(mc)
	  var Translator = null;
	  if(mc.Translate) Translator = mc.Translate;
	  if((mc.parent is MovieClip) && typeof(mc.parent.Translate)!="undefined") Translator = mc.parent.Translate;
	  
	  if(Translator) {
 	    if(mc.comment_text) {
	      mc.comment_text.text = Translator.get_translation(id)
	    }
	  }
	}
	
  }
}