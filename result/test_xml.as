package {
  public class test_xml {	
	[Embed(source="embed/test.xml", mimeType="application/octet-stream")]
	public const testXML:Class;
	public var test;

	function test_xml() {
		XML.ignoreWhitespace = true;
		test = XML(new testXML());		
	}
	
  }
}